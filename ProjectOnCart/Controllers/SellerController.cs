﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectOnCart.Models;

namespace ProjectOnCart.Controllers
{
    public class SellerController : Controller
    {
        // GET: Seller
        public readonly SellerContext context;
        public SellerController(SellerContext context)
        {
            this.context = context;
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Register(Seller s)
        {
            try
            {
                context.Add(s);
                context.SaveChanges();
                ViewBag.message = s.seller_name + " has sucessfully registered";
                //return RedirectToAction("Login");
            }
            catch (Exception )
            {
                ViewBag.message =  " Registration failed";
                return View();
            }
            return View();
        }

             [HttpGet]
            public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(Seller s)
        {
            var log = context.seller.Where(e => e.seller_name == s.seller_name && e.pswd == s.pswd).ToList();
            if(log.Count==0)
            {
                ViewBag.message = "invalid credentials. login again";
                return View();
            }
            else
            {
                HttpContext.Session.SetString("uname", s.seller_name);
                return RedirectToAction("Index");
            }
            
        }
        public IActionResult DashBoard()
        {
            if (HttpContext.Session.GetString("uname") != null)
            {
                ViewBag.uname = HttpContext.Session.GetString("uname").ToString();
                //ViewBag.lstlgn = HttpContext.Session.GetString("lastlogin".ToString());
                if (Request.Cookies["lastlogin"] != null)
                    ViewBag.lstlgn = Request.Cookies["lastlogin"].ToString();
            }
            return View();
        }

        public bool IsExist(string seller_email)
        {
            var result = context.seller.Where(e => e.seller_email == seller_email).ToList();
            if (result.Count == 0)
                return true;
            else
                return false;
        }



        public ActionResult Index()
        {
            return View();
        }

        // GET: Seller/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Seller/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Seller/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Seller/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Seller/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Seller/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Seller/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}