﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectOnCart.Models;

namespace ProjectOnCart.Controllers
{
    public class BuyerController : Controller
    {
        // GET: Buyer
        public readonly BuyerContext context;
        public BuyerController(BuyerContext context)
        {
            this.context = context;
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Register(Buyer b)
        {
            try
            {
                context.Add(b);
                context.SaveChanges();
                ViewBag.message = b.buyer_name + " Has successfully registred";
            }
            catch (Exception )
            {
                ViewBag.message = "Registration failed";
                return View();
            }
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(Buyer b)
        {
            var log = context.buyer.Where(e => e.buyer_name==b.buyer_name && e.buyer_pswd == b.buyer_pswd).ToList();
            if (log.Count == 0)
            {
                ViewBag.message = "login failed. please login again";
                return View();
            }
            else {
                HttpContext.Session.SetString("uname", b.buyer_name);
               // HttpContext.Session.SetString("lastlogin", DateTime.Now.ToString());
                return RedirectToAction("DashBoard");
             }
           
        }


         public IActionResult DashBoard()
        {
            if (HttpContext.Session.GetString("uname") != null)
            {
                ViewBag.uname = HttpContext.Session.GetString("uname").ToString();
                //ViewBag.lstlgn = HttpContext.Session.GetString("lastlogin".ToString());
                if (Request.Cookies["lastlogin"] != null)
                    ViewBag.lstlgn = Request.Cookies["lastlogin"].ToString();
            }
            return View();
        }
        public bool IsExist(string buyer_email)
        {
            var result = context.buyer.Where(e => e.buyer_email == buyer_email).ToList();
            if (result.Count == 0)
                return true;
            else
                return false;
        }

        public IActionResult logout()
        {
            Response.Cookies.Append("lastlogin", DateTime.Now.ToString());
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }


        public ActionResult Index()
        {
            return View();
        }

        // GET: Buyer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Buyer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Buyer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Buyer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Buyer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Buyer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Buyer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}