﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectOnCart.Migrations.Item
{
    public partial class intialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "item",
                columns: table => new
                {
                    item_no = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    item_name = table.Column<string>(nullable: true),
                    item_desc = table.Column<string>(nullable: true),
                    no_of_items = table.Column<int>(nullable: false),
                    price = table.Column<double>(nullable: false),
                    subcategory_id = table.Column<int>(nullable: false),
                    subcategory_name = table.Column<string>(nullable: true),
                    category_id = table.Column<int>(nullable: false),
                    category_name = table.Column<string>(nullable: true),
                    seller_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_item", x => x.item_no);
                });

            migrationBuilder.CreateTable(
                name: "subCategories",
                columns: table => new
                {
                    subcategory_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    category_id = table.Column<int>(nullable: false),
                    category_name = table.Column<string>(nullable: true),
                    subcategory_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subCategories", x => x.subcategory_id);
                });
             
             
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "item");

            migrationBuilder.DropTable(
                name: "subCategories");
        }
    }
}
