﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectOnCart.Migrations
{
    public partial class intialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "seller",
                columns: table => new
                {
                    seller_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    seller_name = table.Column<string>(nullable: true),
                    seller_email = table.Column<string>(nullable: true),
                    seller_phn = table.Column<string>(nullable: true),
                    seller_cpname = table.Column<string>(nullable: true),
                    GSTIN = table.Column<string>(nullable: true),
                    pswd = table.Column<string>(nullable: true),
                    conform_pswd = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller", x => x.seller_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "seller");
        }
    }
}
