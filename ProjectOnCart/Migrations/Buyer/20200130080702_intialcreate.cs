﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectOnCart.Migrations.Buyer
{
    public partial class intialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "buyer",
                columns: table => new
                {
                    buyer_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    buyer_name = table.Column<string>(nullable: true),
                    buyer_email = table.Column<string>(nullable: true),
                    buyer_phnno = table.Column<string>(nullable: true),
                    buyer_pswd = table.Column<string>(nullable: true),
                    conformpswd = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_buyer", x => x.buyer_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "buyer");
        }
    }
}
