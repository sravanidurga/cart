﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOnCart.Models
{
    public class Category
    {
       
        public int category_id { get; set; }
        public string category_name { get; set; }
        public Category(int category_id,string category_name)
        {
            this.category_id = category_id;
            this.category_name = category_name;
        }
    }
}
