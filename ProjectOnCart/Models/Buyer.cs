﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOnCart.Models
{
    public class Buyer
    {
        [Key]
        public int buyer_id { get; set; }
        [Required(ErrorMessage ="enter name")]
        public string buyer_name { get; set; }

        [EmailAddress(ErrorMessage ="invalid email")]
       // [ValidationDomain(allowdomain: "gmail.com", ErrorMessage = "ex:example@gmail.com")]
        [Remote(action: "IsExist", controller: "Buyer", ErrorMessage = "email exist")]
        public string buyer_email { get; set; }

        [RegularExpression(@"[6-9]\d{9}", ErrorMessage = "invalid mobile no")]
        public string buyer_phnno { get; set; }

        [Required(ErrorMessage = "password is required")]
        [RegularExpression("[a-z0-9]{6,8}", ErrorMessage = "password must contain digits,alphabets")]
        [DataType("password")]
        public string buyer_pswd { get; set; }

        [Compare("buyer_pswd", ErrorMessage = "password mismatch")]
        [DataType("password")]
        public string conformpswd { get; set; }
    }
}
