﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectOnCart.Models;
namespace ProjectOnCart.Models
{
    public class ValidationDomainAttribute : ValidationAttribute
    {
        private string allowdomain;
        public ValidationDomainAttribute(string allowdomain)
        {
            this.allowdomain = allowdomain;
        }

        public override bool IsValid(object value)
        {
            string[] email1 = value.ToString().Split("@");
            if (email1[1] == allowdomain)
                return true;
            else return false;
        }
    }
}