﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOnCart.Models
{
    public class Item
    {
        [Key]
       public int item_no { get; set; }
        public string item_name { get; set; }
        public string item_desc { get; set; }

        public int no_of_items { get; set; }
        public double price { get; set; }
        public int subcategory_id { get; set; }
        public string subcategory_name { get; set; }

        public int category_id { get; set; }
        public string category_name { get; set; }
        public string seller_name { get; set; }
        public Item()
        {

        }
        public Item(int item_no, string item_name, string item_desc, int no_of_items, double price, int subcategory_id, string subcategory_name,
           int category_id, string category_name, string seller_name)
        {
            this.item_no = item_no;
            this.item_name = item_name;
            this.item_desc = item_desc;
            this.no_of_items = no_of_items;
            this.price = price;
            this.subcategory_id = subcategory_id;
            this.subcategory_name = subcategory_name;
            this.category_id = category_id;
            this.category_name = category_name;
            this.seller_name = seller_name;
        }
    }
}
