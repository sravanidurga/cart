﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOnCart.Models
{
    public class Seller
    {
        [Key]

      
        public int seller_id { get; set; }

        [Required(ErrorMessage = "enter name")]
        public string seller_name { get; set; }

        [EmailAddress(ErrorMessage = "invalid email")]
       // [ValidationDomain(allowdomain: "gmail.com", ErrorMessage = "ex:example@gmail.com")]
        [Remote(action: "IsExist", controller: "Seller",ErrorMessage ="email exist")]
        public string seller_email { get; set; }

        [RegularExpression(@"[6-9]\d{9}", ErrorMessage = "invalid mobile no")]
        public string seller_phn { get; set; }

        [Required(ErrorMessage ="enter comapny name")]
        public string seller_cpname { get; set; }

        [Required(ErrorMessage ="enter GSTIN number")]
        public string GSTIN { get; set; }

        [Required(ErrorMessage = "password is required")]
        [RegularExpression("[a-z0-9]{6,8}",ErrorMessage ="password must contain digits,alphabets")]
        [DataType("password")]
        public string pswd { get; set; }

        [Compare("pswd", ErrorMessage = "password mismatch")]
        [DataType("password")]
        public string conform_pswd { get; set; }
    }
}
