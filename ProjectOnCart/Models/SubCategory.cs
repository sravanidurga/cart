﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOnCart.Models
{
    public class SubCategory:Category
    {
        [Key]
        public int subcategory_id { get; set; }
        public string subcategory_name { get; set; }
       
        public SubCategory(int subcategory_id,string subcategory_name,int category_id,string category_name):base(category_id,category_name)
        {
            this.subcategory_id = subcategory_id;
            this.subcategory_name = subcategory_name;
            
        }
    }
}
